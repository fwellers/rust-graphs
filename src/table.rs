#[derive(Eq, Hash)]
struct Position<T, E>(T, E)
where
    T: Eq + std::hash::Hash,
    E: Eq + std::hash::Hash;

impl<T, E> PartialEq for Position<T, E>
where
    T: Eq + std::hash::Hash,
    E: Eq + std::hash::Hash,
{
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0 && self.1 == other.1
    }
}

struct Table<T, E, U>
where
    T: Eq + std::hash::Hash,
    E: Eq + std::hash::Hash,
    U: std::hash::Hash,
{
    entries: std::collections::HashMap<Position<T, E>, U>,
}

impl<T, E, U> Table<T, E, U>
where
    T: Eq + std::hash::Hash,
    E: Eq + std::hash::Hash,
    U: std::hash::Hash,
{
    fn new() -> Self {
        Self {
            entries: std::collections::HashMap::new(),
        }
    }
    fn add_value(self: &mut Self, position: Position<T, E>, value: U) {
        self.entries.insert(position, value);
    }
}
