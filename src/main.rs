use std::collections::HashMap;
use std::hash::Hash;

#[allow(unused_variables)]
mod table;
fn main() {
    println!("Hello, world!");
    let mut graph: Box<dyn Graph<i32>> = Box::new(DirectedGraph::new());
    graph.add_vertex(&1,&[2,3]);
    graph.add_vertex(&2,&[3,4]);
    graph.add_vertex(&4,&[5]);
}

trait Graph<'a, T> {
    fn add_vertex(&mut self, vertex: &'a T, connected_vertices: &'a [T]) -> ();
    fn remove_vertex(&mut self, vertex: &'a T, connected_vertices: &'a [T]) -> ();
    fn size(&self) -> usize;
    fn print(&self) -> ();
}

trait Matrix<'a, T>
where
    T: Eq + Hash,
{
    fn update_connection(&mut self, position: TableKey<'a, T>, new_value: bool) -> ();
    fn size(& self) -> usize;
    fn print(&self) -> ();
}

struct DirectedGraph<'a, T>
where
    T: Eq + Hash,
{

    matrix: AdjacencyMatrix<'a, T>
}

/// The key for the nodes in a matrix.
#[derive(Eq, Hash)]
struct TableKey<'a, T>
where
    T: Eq + Hash,
{
    from: &'a T,
    to: &'a T,
}

impl<'a, T> PartialEq for TableKey<'a, T>
where
    T: Eq + Hash,
{
    fn eq(&self, other: &Self) -> bool {
        self.from == other.from && self.to == other.to
    }
}

struct AdjacencyMatrix<'a, T>
where
    T: Eq + Hash,
{
    matrix: HashMap<TableKey<'a, T>, bool>,
}

/// Representation of a graph with vertices of type T, using an
/// adjacency matrix to store its vertices and edges.
impl<'a, T> Graph<'a, T> for DirectedGraph<'a, T>
where
    T: Eq + Hash,
{
    fn add_vertex(&mut self, vertex: &'a T, connected_vertices: &'a [T]) -> () {
        for connected_vertex in connected_vertices {
            self.matrix.update_connection(
                TableKey {
                    from: vertex,
                    to: connected_vertex,
                },
                true,
            );
        }
    }
    fn remove_vertex(&mut self, vertex: &'a T, connected_vertices: &'a [T]) -> () {
        for connected_vertex in connected_vertices {
           self.matrix.update_connection(
                TableKey {
                    from: vertex,
                    to: connected_vertex,
                },
                false,
            );
        }
    }
    fn size(&self) -> usize {
        self.matrix.size()
    }
    fn print(&self) -> () {
        self.matrix.print();
    }
}

impl<'a, T> Matrix<'a, T> for AdjacencyMatrix<'a, T>
where
    T: Eq + Hash,
{
    fn size(&self) -> usize {
        self.matrix.len()
    }
    fn update_connection(&mut self, position: TableKey<'a, T>, new_value: bool) -> () {
        self.matrix.insert(position, new_value);
    }
    fn print(&self) -> () {
        for item in self.matrix {
            println!("from: {}, to: {}",item.0.from,item.0.to, item.1);
        }
    }
}

impl<'a, T> DirectedGraph<'a, T>
where
    T: Eq + Hash,
{
    fn new() -> Self {
        DirectedGraph {
            matrix: AdjacencyMatrix::new()
        }
    }
}

impl<'a, T> AdjacencyMatrix<'a, T>
where
    T: Eq + Hash,
{
    fn new() -> Self {
        AdjacencyMatrix {
            matrix: HashMap::new(),
        }
    }
    
}
